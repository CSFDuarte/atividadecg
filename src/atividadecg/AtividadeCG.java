package atividadecg;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class AtividadeCG {

    public static void main(String[] args) {

        // 1.a.i
        double[] vertice3D = vertice3D();
        System.out.printf("(%f %f %f)\n", vertice3D[0], vertice3D[1], vertice3D[2]);

        // 1.a.ii
        int[][] face = face();
        System.out.printf("(%d %d %d), (%d %d %d), (%d %d %d)\n", face[0][0], face[0][1], face[0][2], face[1][0], face[1][1], face[1][2], face[2][0], face[2][1], face[2][2]);
    }

    public static double[] vertice3D() {
        double[] coordVertice3D = new double[3];

        Scanner ler = new Scanner(System.in);

        try {
            FileReader arq = new FileReader("/home/rene/UNIFEI/CG/1.obj");
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine();

            StringTokenizer st = new StringTokenizer(linha);

            int i = 0;
            while (st.hasMoreTokens()) {
                coordVertice3D[i] = Double.parseDouble(st.nextToken());
                i++;
            }

            arq.close();

            return coordVertice3D;
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }

        return null;
    }

    public static int[][] face() {
        int[][] face = new int[3][3];

        Scanner ler = new Scanner(System.in);

        try {
            FileReader arq = new FileReader("/home/rene/UNIFEI/CG/2.obj");
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine();

            StringTokenizer st = new StringTokenizer(linha);
            
            st.nextToken();
            
            int i=0;
            while (st.hasMoreTokens()) {
                String str = st.nextToken();
                
                String[] f = str.split("/");

                for (int j=0; j < 3; j++) {
                    face[i][j] = Integer.parseInt(f[j]);
                }
                i++;
            }

            arq.close();

            return face;
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }

        return null;
    }
    /*
    public static double[] vertice3D(){
        double[] coordVertice3D = new double[3];
        
        Scanner ler = new Scanner(System.in);

        System.out.printf("\nConteúdo do arquivo texto:\n");
        try {
            FileReader arq = new FileReader("/home/rene/UNIFEI/CG/1.obj");
            BufferedReader lerArq = new BufferedReader(arq);

            String linha = lerArq.readLine();
/*
      while (linha != null) {
        System.out.printf("%s\n", linha);
 
        linha = lerArq.readLine(); // lê da segunda até a última linha
      }*//*

            StringTokenizer st = new StringTokenizer(linha);
            
            int i=0;
            while (st.hasMoreTokens()) {
                coordVertice3D[i] = Double.parseDouble(st.nextToken());
                i++;
            }

            System.out.printf("%f %f %f\n", coordVertice3D[0], coordVertice3D[1], coordVertice3D[2]);

            arq.close();
            
            return coordVertice3D;
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }

        return null;
    }*/
}
